<?php

namespace MyUser;

class Module {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap($e) {
        $events = $e->getApplication()->getEventManager()->getSharedManager();
        $events->attach('ZfcUser\Form\Register', 'init', function($e) {
            $form = $e->getTarget();
            $form->remove('email');
            $form->setValidationGroup('username', 'password', 'passwordVerify', 'lol');
            $lol = new \Zend\Form\Element('lol');
            $lol->setLabel('Your lol');
            $lol->setAttributes(array(
                'type' => 'text'
            ));
            $form->add($lol);
        });
        $events->attach('ZfcUser\Form\RegisterFilter', 'init', function($e) {
            $filter = $e->getTarget();
            // Do what you please with the filter instance ($filter)
        });
    }

}
